import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import { Card, Button} from "react-bootstrap";

// Deconstruct
export default function CaourseCard({courseProp}){
	// check to see if the data was passed successfully
	// console.log(props.courseProp.name);
	// Deconsstruct courseProp properties into their own variable
	const {_id, name, description, price, slots} = courseProp;
	// States 
		// State are used to keep track the information realted to individual component.
	// Hooks 
		// Special react defined methods and functions that allows us to do certain tasks in our components
		// Use the state hook for this component to be able o store its state
	// Syntax
		// const [stateName, setStateName] = useState(initialStateValue)
	// useState() is a hook that creates states.
		// useState() returns an array with 2 items:
			// The first itemin the array is the state
			// and the second one is the setter function(to change the initial state)

		// Array destructuring for the useState()
		// const [count, setCount] = useState(0);
		
		// const [seats, setSeats] = useState(10);
/*ACTIVITY SOLUTION
		console.log(useState(0));
		function enroll (){
			if (seats>0){
			setCount(count + 1);
			// console.log("Enrolees" + count);
			setSeats(seats - 1);
			console.log("Seats: "+ seats);
			}
			else{
			alert("No more seats available")
			}
		}
*/

	// const [seats, setSeats] = useState(30);
	// const [isOpen, setIsOpen] = useState(false);

	// function enroll (){
		
	// 	setCount(count + 1);
	// 	// console.log("Enrolees" + count);

	// 	setSeats(seats - 1);
	// 	// console.log("Seats: "+ seats);
	// }
	// function unEnroll (){
		
	// 	setCount(count - 1);
	// 	// console.log("Enrolees" + count);

	// 	// console.log("Seats: "+ seats);
	// }
	// userEffect()
	// useEffect allows us to run a task or an effect, the difference is that with useEffect we can manipulate when it will run
	// Syntax:
		// useEffect(function, [dependency])
	// useEffect(() =>{
	// 	if(seats === 0){
	// 		alert("No more seats available");
	// 		setIsOpen(true);
	// 	}
	// }, [seats]);

	// if the useEffect() does not have a dependency array, it will run on the initial render and whenever a state is set by its set function
	// useEffect(()=>{
	// 	// Runs on every render
	// 	console.log("useEffect Render")
	// })
	// if the useEffect() has a dependency array but empty it will only run on initial render.
	// useEffect(()=>{
	// 	// Run only on the initial render
	// 	console.log("useEffect Render")
	// },[])
	// if the useEffect() has a dependency array and there is state
	// useEffect(()=>{
	// 	// Run only on the initial render
	// 	// And everytime the dependency value will change (seats).
	// 	console.log("useEffect Render")
	// },[seats])

	return (
<Card className="p-3 my-3">
		    <Card.Body>
		        <Card.Title>
		            {name}
		        </Card.Title>
		        <Card.Subtitle>Description: </Card.Subtitle>
		        <Card.Text>
		            {description}
		        </Card.Text>
		        <Card.Subtitle>Price: </Card.Subtitle>
		        <Card.Text>
		        {price}
		        </Card.Text>
		        <Card.Text>
		       		 Slots: {slots}
		        </Card.Text>
	        {/*We will be able to select a specific course through its*/}
		        <Button as={Link} to={`/courses/${_id}`} variant="primary">Details</Button>
		        {/*<Button variant="primary mx-1"onClick={enroll} disabled={isOpen}>Enroll</Button>
		        <Button variant="danger mx-1"onClick={unEnroll} disabled={isOpen}>Unenroll</Button>*/}

		    </Card.Body>
		</Card>
		)
}