import {useState, useEffect} from "react"
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import { UserProvider } from "./UserContext";

import AppNavbar from "./components/AppNavbar";

import Courses from "./pages/Courses";
import CourseView from "./pages/CourseView";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";


import {Container} from "react-bootstrap";
import './App.css';

function App() {
  // To store the user information and will be used for validating if a user is already login on the app or not.
  const [user, setUser] = useState({
    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  }) 

  console.log(user);

  // Function for clearing localStorage on logout
  const unsetUser = () =>{
    localStorage.clear();
  }
  useEffect(()=>{
    console.log(user);
    console.log(localStorage);
  },[user])
  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      //This will be set to the user state.
      if(typeof data._id !== "undefined"){
          setUser({
            //undefined
            id: data._id,
            isAdmin: data.isAdmin
          })
      }
      else{
          setUser({
              //undefined
              id: null,
              isAdmin: null
            })
      }
    })
  }, [])
  return (
    // Rect Fragment does not like rendering two adjacent elements. instead the adjacent element must be wrapped by a parent element/react fragment.
    
    // ReactJS is a single page application (SPA)
    // Router component is used to wrap around all components which will have access to our routing system.
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid>     
            <Routes>
            {/*
                - Route assigns an endpoint and displays the appropriate page component for that endpoint.
                - "path" attribute assigns the endpoint
                - "element" attribute assigns page component to be displayed at the endpoint.
            */}
                <Route exact path = "/" element = {<Home />} />
                <Route exact path = "/courses" element = {<Courses />} />
                <Route exact path = "/courses/:courseId" element = {<CourseView />} />
                <Route exact path = "/register" element = {<Register />} />
                <Route exact path = "/login" element = {<Login />} />
                <Route exact path = "/logout" element = {<Logout />} />
                <Route exact path ="*" element = {<Error />} />               
            </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
