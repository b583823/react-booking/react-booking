// import coursesData from "../data/coursesData";
import {useEffect, useState} from "react"
import CourseCard from "../components/CourseCard";

export default function Courses(){
	// Retrieve the courses from the database upon initial render of the Course Component
	const [courses, setCourses] = useState([])
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			setCourses(data.map(course=>{
				return (
					<CourseCard key={course._id} courseProp={course} />
					)
			}))
		})

	},[])
	// Check if we can access the mock database.
	// console.log(coursesData);
	// Props 
		// is a shorthand for property since components are considered as objects in ReactJS
		// Props is a way to pass data from the parent to child component.
		// it is synonymous to the function parameter.
		// const courses = coursesData.map(course=>{
		// 	return (
		// 		<CourseCard key={course.id} courseProp = {course} />
		// 		)
		// })
	return(
			<>
				<h1>Courses</h1>
				{courses}
			</>
		)
}